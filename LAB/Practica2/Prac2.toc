\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Seguridad en la ejecuci\IeC {\'o}n}{1}% 
\contentsline {section}{\numberline {2}Objetivos de aprendizaje}{1}% 
\contentsline {section}{\numberline {3}Material y Equipo}{2}% 
\contentsline {section}{\numberline {4}Introducci\IeC {\'o}n}{2}% 
\contentsline {section}{\numberline {5}Actividad de investigaci\IeC {\'o}n previa}{2}% 
\contentsline {subsection}{\numberline {5.1}Resistencias Pull Up y Pull Down}{2}% 
\contentsline {subsubsection}{\numberline {5.1.1}Resistencias Pull Up}{2}% 
\contentsline {subsubsection}{\numberline {5.1.2}Resistencias Pull Down}{3}% 
\contentsline {subsubsection}{\numberline {5.1.3}Valor de la resistencia}{3}% 
\contentsline {section}{\numberline {6}Desarrollo}{4}% 
\contentsline {subsection}{\numberline {6.1}Interruptor Sostenido}{4}% 
\contentsline {subsection}{\numberline {6.2}Interruptores moment\IeC {\'a}neos}{4}% 
\contentsline {section}{\numberline {7}Resistores Pull UP y pull Down}{5}% 
\contentsline {section}{\numberline {8}Compuerta NAND}{5}% 
\contentsline {section}{\numberline {9}Cuestionario}{6}% 
\contentsline {subsection}{\numberline {9.1}\IeC {\textquestiondown }Qu\IeC {\'e} es una protoboard?}{6}% 
\contentsline {subsection}{\numberline {9.2}Menciona 3 buenos consejos para tener un buen uso de la protoboard.}{6}% 
\contentsline {subsection}{\numberline {9.3}Caracter\IeC {\'\i }sticas de una Protoboard}{6}% 
\contentsline {section}{\numberline {10}Conclusiones}{6}% 
