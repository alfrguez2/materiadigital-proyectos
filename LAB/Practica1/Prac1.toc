\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Seguridad en la ejecuci\IeC {\'o}n}{1}% 
\contentsline {section}{\numberline {2}Objetivos de aprendizaje}{1}% 
\contentsline {section}{\numberline {3}Material y Equipo}{2}% 
\contentsline {section}{\numberline {4}Introducci\IeC {\'o}n}{2}% 
\contentsline {section}{\numberline {5}Actividad de investigaci\IeC {\'o}n previa}{2}% 
\contentsline {subsection}{\numberline {5.1}Ley de Ohm}{2}% 
\contentsline {subsection}{\numberline {5.2}Primera ley de corrientes de Kirchhoff (L.C.K.)}{2}% 
\contentsline {subsection}{\numberline {5.3}Segunda ley de tensiones de Kirchhoff (L.T.K)}{2}% 
\contentsline {section}{\numberline {6}Desarrollo}{3}% 
\contentsline {subsection}{\numberline {6.1}Circuito}{3}% 
\contentsline {subsection}{\numberline {6.2}Se\IeC {\~n}ales}{4}% 
\contentsline {section}{\numberline {7}Cuestionario}{4}% 
\contentsline {subsection}{\numberline {7.1}\IeC {\textquestiondown }Para qu\IeC {\'e} sirve el osciloscopio?}{4}% 
\contentsline {subsection}{\numberline {7.2}\IeC {\textquestiondown }Qu\IeC {\'e} hace un generador de funciones?}{4}% 
\contentsline {subsection}{\numberline {7.3}Menciona 3 reglas para un buen uso de equipo de laboratorio.}{5}% 
\contentsline {section}{\numberline {8}Conclusiones}{5}% 
\contentsline {section}{\numberline {9}Referencias}{5}% 
