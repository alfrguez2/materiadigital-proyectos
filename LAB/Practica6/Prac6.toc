\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Seguridad en la ejecuci\IeC {\'o}n}{1}% 
\contentsline {section}{\numberline {2}Objetivos de aprendizaje}{1}% 
\contentsline {section}{\numberline {3}Material y Equipo}{2}% 
\contentsline {section}{\numberline {4}Introducci\IeC {\'o}n}{2}% 
\contentsline {section}{\numberline {5}Actividad de investigaci\IeC {\'o}n previa}{2}% 
\contentsline {subsection}{\numberline {5.1}M\IeC {\'e}todo del Dise\IeC {\~n}o Combinacional}{2}% 
\contentsline {section}{\numberline {6}Desarrollo}{3}% 
\contentsline {subsection}{\numberline {6.1}Especificaci\IeC {\'o}n el sistema}{3}% 
\contentsline {subsection}{\numberline {6.2}Determinaci\IeC {\'o}n de las entradas y salidas}{3}% 
\contentsline {subsection}{\numberline {6.3}Tabla de verdad}{3}% 
\contentsline {subsection}{\numberline {6.4}Mapas de Karnaugh}{4}% 
\contentsline {subsection}{\numberline {6.5}Diagrama L\IeC {\'o}gico}{6}% 
\contentsline {subsection}{\numberline {6.6}Implementaci\IeC {\'o}n F\IeC {\'\i }sica}{6}% 
\contentsline {subsection}{\numberline {6.7}Funcionamiento}{6}% 
\contentsline {section}{\numberline {7}Cuestionario}{7}% 
\contentsline {subsection}{\numberline {7.1}\IeC {\textquestiondown }Qu\IeC {\'e} es una compuerta l\IeC {\'o}gica?}{7}% 
\contentsline {subsection}{\numberline {7.2}\IeC {\textquestiondown }Qu\IeC {\'e} es una compuerta AND?}{7}% 
\contentsline {subsection}{\numberline {7.3}\IeC {\textquestiondown }Qu\IeC {\'e} es una compuerta OR?}{8}% 
\contentsline {section}{\numberline {8}Conclusiones}{8}% 
