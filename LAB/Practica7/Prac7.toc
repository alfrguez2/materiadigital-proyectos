\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Seguridad en la ejecuci\IeC {\'o}n}{1}% 
\contentsline {section}{\numberline {2}Objetivos de aprendizaje}{1}% 
\contentsline {section}{\numberline {3}Material y Equipo}{2}% 
\contentsline {section}{\numberline {4}Actividad de investigaci\IeC {\'o}n previa}{2}% 
\contentsline {subsection}{\numberline {4.1}Suma Binaria}{2}% 
\contentsline {subsection}{\numberline {4.2}Resta Binaria}{2}% 
\contentsline {subsection}{\numberline {4.3}Complemento a 1}{3}% 
\contentsline {subsubsection}{\numberline {4.3.1}Complemento a 2}{3}% 
\contentsline {section}{\numberline {5}Introducci\IeC {\'o}n}{3}% 
\contentsline {section}{\numberline {6}Desarrollo}{3}% 
\contentsline {subsection}{\numberline {6.1}Suma Binaria}{3}% 
\contentsline {subsection}{\numberline {6.2}Diagrama L\IeC {\'o}gico}{4}% 
\contentsline {subsection}{\numberline {6.3}Circuito}{4}% 
\contentsline {section}{\numberline {7}Cuestionario}{5}% 
\contentsline {subsection}{\numberline {7.1}\IeC {\textquestiondown }Qu\IeC {\'e} es un sumador?}{5}% 
\contentsline {subsection}{\numberline {7.2}\IeC {\textquestiondown }Qu\IeC {\'e} es un restador?}{6}% 
\contentsline {subsection}{\numberline {7.3}\IeC {\textquestiondown }Qu\IeC {\'e} es un medio sumador o semisumador? }{6}% 
\contentsline {section}{\numberline {8}Conclusiones}{7}% 
